RESTful API to import CSV data
==============================

### The functionality of this project is to create a Django RESTful API that imports images from the contents of a csv url.



* django_rest_framework form to import csv_url
* serialized CSVUrl and Image model and created Views
* import_csv django custom command for processing and importing image instances to database from csv url
* command tests and tox

