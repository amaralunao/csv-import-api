from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from csvapi import views

router = routers.DefaultRouter()
router.register(r'images', views.ImageViewSet)
router.register(r'csvurl', views.CSVUrlViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    ]
