from django.db import models


class CSVUrl(models.Model):
    csv_url = models.URLField()
    import_errors = models.TextField(blank=True)


class Image(models.Model):
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    image = models.URLField(max_length=255)
    url = models.ForeignKey('CSVUrl', related_name='images', blank=True,
                            null=True)

    def __str__(self):
        return self.title
