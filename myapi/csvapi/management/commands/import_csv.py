from django.core.management.base import BaseCommand, CommandError

from csvapi.models import Image, CSVUrl
from csvapi.utils import process_csv
from csvapi.serializers import ImageSerializer


class Command(BaseCommand):
    help = "Imports the image instances to the database"

    def add_arguments(self, parser):
        parser.add_argument('csv_id', type=int)

    def handle(self, *args, **options):
        csv_id = options['csv_id']
        csv_url = CSVUrl.objects.get(pk=csv_id)
        csv_dict = process_csv(csv_url.csv_url)
        errors = self.import_csv(csv_dict, csv_id)
        csv_url.import_errors = ", ".join(errors)
        csv_url.save()
        if errors:
            for row in errors:
                self.stderr.write(self.style.NOTICE(row))
        self.stdout.write(self.style.SUCCESS(
            "Successfuly imported CSV file to database"))

    def import_csv(self, csv_dict, csv_id):
        clean = []
        errors = []
        for row in csv_dict:
            new_row = ImageSerializer(data=row)
            if new_row.is_valid():
                clean.append(row)
            else:
                errors.append('%s Error:%s\n' % (row, new_row.errors))
        Image.objects.bulk_create([Image(url_id=csv_id, **x)for x in clean])
        return errors
