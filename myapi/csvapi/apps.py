from django.apps import AppConfig


class CsvapiConfig(AppConfig):
    name = 'csvapi'
