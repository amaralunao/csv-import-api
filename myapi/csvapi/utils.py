import csv
import codecs

from six.moves import urllib


def process_csv(csv_url):
    try:
        response = urllib.request.urlopen(csv_url)
    except Exception:
        return False
    decoder = codecs.getreader("utf-8")
    reader = csv.DictReader(decoder(response))
    return reader
