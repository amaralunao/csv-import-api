from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from csvapi.models import CSVUrl, Image
from csvapi.serializers import ImageSerializer, CSVUrlSerializer


class CSVUrlViewSet(ModelViewSet):
    queryset = CSVUrl.objects.all()
    serializer_class = CSVUrlSerializer
    parser_classes = (FormParser, MultiPartParser)


class ImageViewSet(ReadOnlyModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
