import csv
import codecs

from six.moves import urllib
from rest_framework import serializers
from PIL import Image as im

from csvapi.models import CSVUrl, Image
from csvapi.utils import process_csv


class CSVUrlSerializer(serializers.ModelSerializer):
    import_errors = serializers.CharField(read_only=True)
    images = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='image-detail'
    )

    def create(self, validated_data):
        csv_instance = super(CSVUrlSerializer, self).create(validated_data)
        reader = process_csv(validated_data['csv_url'])
        clean_data = []
        errors = []
        for row in reader:
            image_instance = ImageSerializer(data=row)
            if image_instance.is_valid():
                image_instance.save(url=csv_instance)
            else:
                errors.append('%s Error:%s ' % (row, image_instance.errors))
        csv_instance.import_errors = ", ".join(errors)
        csv_instance.save()
        return csv_instance

    def validate_csv_url(self, value):
        reader = process_csv(value)
        fieldnames = ['title', 'description', 'image']
        if not reader:
            raise serializers.ValidationError("Url Not found")
        if fieldnames != reader.fieldnames:
            raise serializers.ValidationError(
                "This is not a valid csv url or fieldnames are not "
                "assigned correctly")
        return value

    class Meta:
        model = CSVUrl
        fields = ('csv_url', 'id', 'images', 'import_errors')


class ImageSerializer(serializers.HyperlinkedModelSerializer):

    def validate_image(self, value):
        try:
            image_url = urllib.request.urlopen(value)
            im.open(image_url)
            return value
        except Exception as e:
            raise serializers.ValidationError("Image url error: %s" % e)

    class Meta:
        model = Image
        fields = ('title', 'description', 'image', 'url')
