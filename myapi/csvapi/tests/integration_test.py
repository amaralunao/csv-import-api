from django.test import TestCase, Client
import pytest


from csvapi.models import CSVUrl, Image


@pytest.mark.django_db
class TestCSVUrl:

    def test_csv_url_list_get(self, client, csv_url):
        response = client.get('/csvapi/csvurl/')
        results = response.data['results'][0]
        assert results['id'] == csv_url.id
        assert results['csv_url'] == csv_url.csv_url
        assert response.status_code == 200

    def test_csv_url_detail_get_found(self, csv_url, client):
        latest_url = csv_url.id
        response = client.get('/csvapi/csvurl/%s/' % latest_url)
        assert response.data['id'] == csv_url.id
        assert response.data['csv_url'] == csv_url.csv_url
        assert response.status_code == 200

    def test_csv_url_detail_get_not_found(self, csv_url, client):
        latest_url = csv_url.id
        response = client.get('/csvapi/csvurl/%s/' % (latest_url+1))
        assert response.status_code == 404

    def test_images_list_get(self, image, client):
        response = client.get('/csvapi/images/')
        results = response.data['results'][0]
        assert results['title'] == image.title
        assert results['image'] == image.image
        assert response.status_code == 200

    def test_images_detail_get_found(self, image, client):
        latest_image = image.id
        response = client.get('/csvapi/images/%s/' % latest_image)
        assert response.data['title'] == image.title
        assert response.data['image'] == image.image
        assert response.status_code == 200

    def test_images_detail_get_not_found(self, image, client):
        latest_image = image.id
        response = client.get('/csvapi/images/%s/' % (latest_image+1))
        assert response.status_code == 404

    def test_csv_url_list_post_correct(self, csv_url, client):
        response = client.post('/csvapi/csvurl/', {'csv_url': csv_url.csv_url})
        assert response.status_code == 201

    def test_csv_url_list_post_random_url(self, client):
        response = client.post('/csvapi/csvurl/', {'csv_url':
                                                   'https://google.com'})
        assert response.status_code == 400
        assert response.data == {'csv_url': ['This is not a valid csv ' +
                                 'url or fieldnames are not ' +
                                             'assigned correctly']}

    def test_csv_url_list_post_empty(self, client):
        response = client.post('/csvapi/csvurl/', {'csv_url':
                                                   ''})
        assert response.status_code == 400
        assert response.data == {'csv_url': ['This field may not be blank.']}

    def test_csv_url_list_post_not_url(self, client):
        response = client.post('/csvapi/csvurl/', {'csv_url':
                                                   'heya'})
        assert response.status_code == 400
        assert response.data == {'csv_url': ['Enter a valid URL.']}
