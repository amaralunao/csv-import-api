import csv

from django.core.management import call_command
import pytest
from pytest import raises

from csvapi.models import Image, CSVUrl
from csvapi.tests.helper import main_csv_url


@pytest.mark.django_db
class TestAdminCommand:
    def test_import_csv_image_error(self, command, csv_url):
        errors = command.import_csv([{'title': '1', 'description': '12',
                                    'image': ''}], '1')
        assert len(errors) is 1
        assert Image.objects.count() is 0

    def test_import_csv_title_error(self, command, csv_url):
        errors = command.import_csv([{'title': '1'*102, 'description': '12',
                                    'image': 'https://www.google.com'}], '1')
        assert len(errors) is 1
        assert Image.objects.count() is 0

    def test_import_csv_correct(self, command, csv_url, image):
        errors = command.import_csv([{'title': '1', 'description': '12'*300,
                                     'image': image.image}], '1')
        assert len(errors) is 0
        assert Image.objects.count() is 2


@pytest.mark.django_db
class TestIntegrationCommand:
    def test_integration_command(self):
        url_id = str(CSVUrl.objects.create(csv_url=main_csv_url).id)
        call_command('import_csv', url_id)
        # test that all images are imported
        assert Image.objects.count() is 11
        # test that the foreign key is correctly assigned
        url_id_2 = str(CSVUrl.objects.create(csv_url=main_csv_url).id)
        call_command('import_csv', url_id_2)
        assert Image.objects.filter(url=url_id).count() is 11
        assert Image.objects.filter(url=url_id_2).count() is 11
        assert Image.objects.count() is 22
        # test is errors are correctly imported
        instance = CSVUrl.objects.latest('id')
        assert instance.import_errors
