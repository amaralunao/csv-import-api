from django.test import Client
import pytest
from pytest_factoryboy import register
import factory

from csvapi.models import CSVUrl, Image
from csvapi.serializers import CSVUrlSerializer, ImageSerializer
from csvapi.management.commands.import_csv import Command
from csvapi.tests.factories import CSVUrlFactory, ImageFactory
from csvapi.tests.helper import main_404_url


@pytest.fixture(scope='function')
def command(request):
    return Command()


@pytest.fixture(scope='function')
def csv_serializer(request):
    return CSVUrlSerializer()


@pytest.fixture(scope='function')
def image_serializer(request):
    return ImageSerializer()

register(CSVUrlFactory)
register(ImageFactory)

register(ImageFactory, 'url_not_found', image=main_404_url)
