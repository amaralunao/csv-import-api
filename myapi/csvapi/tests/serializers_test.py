import io
import csv
import unittest
import mock

from rest_framework import serializers
from pytest import raises
import pytest
import six

from csvapi.models import CSVUrl, Image
from csvapi.utils import process_csv
from csvapi.serializers import CSVUrlSerializer
from csvapi.tests.helper import main_csv_url, main_image_url, main_404_url


def test_process_csv_correct_url():
    assert isinstance(process_csv(main_csv_url), csv.DictReader)


def test_process_csv_random_url():
    assert isinstance(process_csv('http://google.com'), csv.DictReader)


def test_process_csv_url_notfound():
    assert process_csv('http://bla.blbla') is False


@mock.patch('csvapi.serializers.process_csv')
@pytest.mark.django_db
class TestCSVSerializer:

    def test_create_super(self, mock_process_csv, csv_serializer):
        mock_process_csv.return_value = [{'image': 'j'}]
        csv_serializer.create({'csv_url': main_csv_url})
        assert CSVUrl.objects.count() is 1

    def test_create_import_errors_valid_and_errors(self, mock_process_csv,
                                                   csv_serializer):
        mock_process_csv.return_value = [{'image': ' '},
                                         {'image': main_image_url}]
        csv_serializer.create({'csv_url': main_csv_url})
        instance = CSVUrl.objects.latest('pk')
        assert 'This field may not be blank' in str(instance.import_errors)
        assert Image.objects.get(image=main_image_url)

    def test_create_import_errors_empty(self, mock_process_csv,
                                        csv_serializer):
        mock_process_csv.return_value = [{'image': ''}]
        csv_serializer.create({'csv_url': main_csv_url})
        instance = CSVUrl.objects.latest('pk')
        assert not Image.objects.count()
        assert 'This field may not be blank' in str(instance.import_errors)

    def test_validate_csv_wrong_fieldnames(self, mock_process_csv,
                                           csv_serializer):
        csv_wrong_fieldnames = six.StringIO("Head1,Head2,images\r\n 1,2,3")
        mock_process_csv.return_value = csv.DictReader(csv_wrong_fieldnames)
        with raises(serializers.ValidationError):
            csv_serializer.validate_csv_url('http://google.com')

    def test_validate_csv_not_comma_seperated(self, mock_process_csv,
                                              csv_serializer):
        csv_not_comma_seperated = six.StringIO("title;description;image\r\n" +
                                               "1;2;3")
        mock_process_csv.return_value = csv.DictReader(csv_not_comma_seperated)
        with raises(serializers.ValidationError):
            csv_serializer.validate_csv_url('http://google.com')

    def test_validate_csv_not_found(self, mock_process_csv, csv_serializer):
        mock_process_csv.return_value = False
        with raises(serializers.ValidationError):
            csv_serializer.validate_csv_url('http://ghh.hsh')

    def test_validate_csv_success_story(self, mock_process_csv,
                                        csv_serializer):
        csv_correct = six.StringIO("title,description,image\r\n1,2,3")
        mock_process_csv.return_value = csv.DictReader(csv_correct)
        assert csv_serializer.validate_csv_url('http://google.com')


@pytest.mark.django_db
class TestImageSerializer:
    def test_validate_image_not_found(self, image_serializer):
        with raises(serializers.ValidationError) as e:
            image_serializer.validate_image(main_404_url)
        assert 'Image url error: HTTP Error 404: Not Found' in str(e.value)

    def test_validate_image_no_image(self, image_serializer):
        with raises(serializers.ValidationError) as e:
            image_serializer.validate_image('http://google.com')
        assert 'Image url error: cannot identify image file' in str(e.value)

    def test_validate_image_success_story(self, image_serializer):
        assert image_serializer.validate_image(main_image_url)
